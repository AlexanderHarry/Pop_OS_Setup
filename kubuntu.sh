#!/bin/bash
DIRECTORY="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
FILE=$(basename $BASH_SOURCE)
cd
mkdir ~/Desktop/Programs/
PROGRAM_FOLDER='Desktop/Programs/'

# Print formated output
print_good_output() {
  echo -e "
\e[36m---------- $1 ----------\e[m
"
}

# Print red formated output
print_error_output() {
  echo -e "
\e[1;31m---------- $1 ----------\033[0m
"
}

# Print unformated output with an uncolored link
print_no_format_link() {
  echo -e "\e[36m$1 \e[m" $2
}

# Print unformated output without a link
print_no_format() {
  echo -en "\e[36m$1 \e[m"
}

# Get the user input for programs wanted
user_input() {

  if $INSTALL_Jetbrains_Toolbox; then
    print_good_output "Jetbrains Toolbox"
    print_no_format_link 'Copy the link address of the "direct link" button link from:' https://www.jetbrains.com/toolbox-app/download/download-thanks.html
    print_no_format 'Paste link address here:'
    read JETBRAINS_TOOLBOX

    print_no_format 'Copy the link address of the "SHA-256 checksum" button link.'
    print_no_format 'Paste link address here:'
    read JETBRAINS_TOOLBOX_CHECKSUM
  fi

  if $INSTALL_Anaconda; then
    print_good_output "Anaconda"
    print_no_format_link 'Copy the link address of the "Download" button link from:' https://www.anaconda.com/distribution/#linux
    print_no_format 'Paste link address here:'
    read ANACONDA
    anaconda_version="$(echo "$ANACONDA" | grep -Poe 'Anaconda.*')"
    print_no_format_link 'Copy the sha256 of the your appropriate Anaconda download.' https://docs.anaconda.com/anaconda/install/hashes/"$anaconda_version"-hash/
    print_no_format 'Paste sha256 here:'
    read ANACONDA_CHECKSUM
    # Clear any spaces accidently copied in the checksum
    ANACONDA_CHECKSUM="$(echo "$ANACONDA_CHECKSUM" | grep -Poe '\S.*\S')"
  fi

  if $INSTALL_Slack; then
    print_good_output "Slack"

    echo -e '\e[36mCopy the link address of the "Try again" button link from:\e[m' https://slack.com/downloads/instructions/fedora
    echo -e '\e[36mPaste link address here:\e[m'
    read SLACK
  fi

  install_if_selected $INSTALL_Brother_Printer_Driver brother_printer_setup

  print_good_output "Github Setup"
  print_no_format "What is your name?:"
  read GITHUB_USER_NAME

  print_no_format "What is your email?:"
  read GITHUB_USER_EMAIL
}

# Asks user what packages they want installed
programs_wanted() {

  for package in "$@"; do
    while true; do
      print_no_format "Do you want to install "$package"? [y/n/N]"
      echo ""
      read choice
      if [[ $choice = 'y' ]]; then
        eval "INSTALL_${package}"=true
        break
      elif [[ -z $choice ]] || [[ $choice = 'N' ]] || [[ $choice = 'n' ]]; then
        eval "INSTALL_${package}"=false
        break
      else
        print_error_output "Enter 'y' for yes or 'n' for no"
      fi
    done
  done
}

# Brother Printer Setup
brother_printer_setup() {
  print_good_output "Brother Printer Setup"
  print_no_format_link 'Vist the following link. Search for your model.  Choose the "Driver Install Tool".  Read and agree to the license. Copy the link address of "If your download does not start automatically, please click here.":' 'https://support.brother.com/g/b/productsearch.aspx?c=us&lang=en&content=dl'
  print_no_format 'Paste link address here:'
  read BROTHER_DRIVER
  print_no_format 'Enter your printer model:'
  read BROTHER_MODEL

}

# Upgrades the packages of the freshly installed systems
initial_package_upgrade() {
  print_good_output "Upgrading packages"
  sudo apt upgrade && sudo apt update -y
}

# Removes all packages passed into the function
remove_package() {
  # source for $@: https://stackoverflow.com/questions/255898/how-to-iterate-over-arguments-in-a-bash-script, user Robert Gamble
  for package in "$@"; do
    print_good_output "Removing package $package"
    sudo apt remove $package -y
  done
}

# Installs all packages passed into the function
install_package() {
  # source for $@: https://stackoverflow.com/questions/255898/how-to-iterate-over-arguments-in-a-bash-script, user Robert Gamble
  for package in "$@"; do
    if [[ "$package" != '' ]]; then
      print_good_output "Installing package $package"
      sudo apt update && sudo apt install -y $package
    fi
  done
}

install_flatpak() {
  sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  sudo flatpak install flathub $1 -y
}

install_code() {

curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
install_package code
}

install_spotify() {
curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add - 
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
install_package spotify-client
}

install_brave() {

curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -

echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list

install_package brave-browser
}

# Method to install wanted programs
install_wanted_packages() {
  install_brave
  install_package  docker.io apt-transport-https curl libreoffice openjdk-11-jdk xdotool git-lfs thunderbird scribus
  install_if_selected $INSTALL_Slack install_slack
  install_if_selected $INSTALL_Jetbrains_Toolbox install_jetbrains_toolbox
  install_if_selected $INSTALL_Spotify install_package_license_agreements "Spotify" https://www.spotify.com/us/legal/end-user-agreement/ install_spotify
  install_if_selected $INSTALL_VS_Code install_package_license_agreements "Visual Studio Code" https://code.visualstudio.com/License install_code
  install_if_selected $INSTALL_Anaconda install_anaconda
  install_if_selected $INSTALL_Brother_Printer_Driver install_brother_printer
  install_nvidia
}

# Install packages with license agreements
install_package_license_agreements() {
  accepted=''
  while [[ $accepted != 'yes' || $accepted != 'no' ]]; do

    print_no_format "${1} Requires Accepting a License."
    print_no_format "
You must read and accept this license to install and use ${1}."
    print_no_format "
If you do not accept, ${1} will not be installed."
    print_no_format_link "
Read the ${1} license:" ${2}
    print_no_format "
I've read and accept the ${1} license: [yes/no]"
    read accepted
    if [[ $accepted = 'yes' || $accepted = 'no' ]]; then
      break
    else
      print_error_output "You must type 'yes' or 'no' to the "$1" license"
    fi
  done

  if [ $accepted = 'yes' ]; then
    print_good_output "Installing ${1}"
    "$3" "$4"
  else
    print_error_output "${1} will not be installed"
  fi
}

install_slack() {
  #Install slack
  install_package libappindicator-gtk3
  print_good_output "Installing Slack"

  wget "$SLACK"
  sudo rpm -i slack*.rpm
  rm slack*.rpm
}

# Install the jetbrains toolbox
install_jetbrains_toolbox() {

  print_good_output "Installing Jetbrains Toolbox"

  # Get Jetbrains toolbox
  wget "$JETBRAINS_TOOLBOX" -P $PROGRAM_FOLDER
  wget "$JETBRAINS_TOOLBOX_CHECKSUM" -P $PROGRAM_FOLDER

  cd $PROGRAM_FOLDER

  # Verify Jetbrains toolbox checksum
  if [[ "$(sha256sum -c jetbrains*.sha256)" == "jetbrains-toolbox"*"OK" ]]; then
    print_good_output "Jetbrains checksum OK"
    tar -xvf jetbrains*.tar.gz

  else
    print_error_output "BAD JETBRAINS CHECKSUM"
    print_error_output "Jetbrains toolbox will not install."
  fi

  rm jetbrains*.tar.gz jetbrains*.sha256
}

# Install Anaconda
install_anaconda() {
  print_good_output "Installing Anaconda"
  cd
  # Get Anaconda
  wget "$ANACONDA" -P $PROGRAM_FOLDER
  cd $PROGRAM_FOLDER

  # Verify Anaconda checksum
  if [[ "$(
    echo "$ANACONDA_CHECKSUM" "Anaconda"*".sh" | sha256sum --check
  )" == "Anaconda"*".sh"*"OK" ]]; then
    print_good_output "Anaconda checksum OK"
    bash Anaconda*.sh

  else
    print_error_output "BAD ANACONDA CHECKSUM"
    print_error_output "Anaconda will not install."
  fi

  rm Anaconda*.sh
}

# Configures the git config settings
git_config() {
  git config --global user.name "$GITHUB_USER_NAME"
  git config --global user.email "$GITHUB_USER_EMAIL"
  git config --global credential.helper cache
  git config --global credential.helper "cache --timeout=3600"
}

# Installs additional cuda libraries for nvidia
install_nvidia() {
  # Install NVIDIA Drivers
  if [[ $(lspci | grep -E "VGA|3D") == *"NVIDIA"* ]]; then
    print_good_output "NVIDIA Graphics Found"
    sudo ubuntu-drivers autoinstall
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    install_package nvidia-container-toolkit
    sudo systemctl restart docker
  fi
}

# Runs through the brother printer installer
install_brother_printer() {
  print_good_output "Installing Brother Printer"
  wget "$BROTHER_DRIVER"
  gunzip linux-brprinter-installer-*.*.*-*.gz
  sudo bash linux-brprinter-installer-*.*.*-* $BROTHER_MODEL
  rm linux-brprinter-installer* brscan*.deb cupswrapper*.deb mfc*.deb

}

# Checks if user has chosen a program and runs the command
install_if_selected() {
  if $1; then
    $2 "$3" "$4" "$5" $6
  fi
}

# Sets up bash config
setup_bash() {
  # Clone the bash_it repo
  print_good_output "Setting up bash settings"
  git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
  ~/.bash_it/install.sh <<EOF
N
EOF

  # Add blinking I-beam cursor
  echo "
# Add blinking ibeam cursor
echo -ne '\e[5 q'
" >>~/.bashrc

  # Sets the desired bash_it theme
  sed -i -e "s/export BASH_IT_THEME='bobby'/export BASH_IT_THEME='powerline-plain'/g" ~/.bashrc
}

# Opens jetbrains-toolbox to create icon launcher.  Opens code to configure settings.
open_files() {
  if $INSTALL_Jetbrains_Toolbox; then
    cd ~/$PROGRAM_FOLDER/jetbrains*
    ./jetbrains-tool*
    sleep 3
    xdotool windowminimize $(xdotool getactivewindow)
  fi
  if $INSTALL_VS_Code; then
    code
    sleep 3
    xdotool windowminimize $(xdotool getactivewindow)
  fi
}

# A countdown for reboot.
reboot_countdown() {
  # Reboot system
  for ((countdown = 20; countdown >= 1; countdown--)); do
    echo -n -e "\r\e[31m---------- Installer Finished - Rebooting in $countdown seconds ----------\e[m"
    sleep 1
  done
  reboot
}

# Sets VS code settings to auto save, removes telemetry, and sets the color scheme
code_settings() {
  echo '{
    "files.autoSave": "afterDelay",
    "telemetry.enableCrashReporter": false,
    "telemetry.enableTelemetry": false,
    "workbench.colorTheme": "Atom One Dark",
    "workbench.startupEditor": "none",
    "workbench.iconTheme": "material-icon-theme",
    "editor.fontSize": 13,
    "material-icon-theme.showWelcomeMessage": false
}' >>~/.config/Code/User/settings.json
  code --install-extension ms-python.anaconda-extension-pack
  code --install-extension formulahendry.auto-close-tag
  code --install-extension formulahendry.auto-rename-tag
  code --install-extension aaron-bond.better-comments
  code --install-extension kamikillerto.vscode-colorize
  code --install-extension batisteo.vscode-django
  code --install-extension eamodio.gitlens
  code --install-extension redhat.java
  code --install-extension christian-kohler.path-intellisense
  code --install-extension esbenp.prettier-vscode
  code --install-extension ms-python.python
  code --install-extension coenraads.bracket-pair-colorizer-2
  code --install-extension Shan.code-settings-sync
  code --install-extension redhat.vscode-yaml
  code --install-extension pkief.material-icon-theme
  code --install-extension visualstudioexptteam.vscodeintellicode
  code --install-extension ms-azuretools.vscode-docker
  code --install-extension vscjava.vscode-java-test
  code --install-extension streetsidesoftware.code-spell-checker
  code --install-extension akamud.vscode-theme-onedark
  code --install-extension foxundermoon.shell-format
}

# Changes KDE settings
kde_settings() {
  lookandfeeltool -a 'org.kde.breezedark.desktop'
}

# Driver to run the script
driver() {
  print_good_output "Lets Get Started!"
  programs_wanted "Spotify" "Jetbrains_Toolbox" "Slack" "VS_Code" "Anaconda" "Brother_Printer_Driver"
  user_input
  initial_package_upgrade
  remove_package k3b
  kde_settings
  install_wanted_packages
  open_files
  install_if_selected $INSTALL_VS_Code code_settings
  git_config
  setup_bash
  #Remove this script file
  cd $DIRECTORY
  rm $FILE
  reboot_countdown
}

driver
