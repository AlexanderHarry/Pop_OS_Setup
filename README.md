# Introduction
This script was written to reduce the time to setup a new installation of Pop!_OS and Fedora to my preferences.  This script helps automate some mundane tasks such as setting up the .gitconfig file and disabling telemetry from VS Code. 

# Running the script on a fresh install of Pop!_OS
To run this script:

1. Download the script.
2. Navigate to the directory of the script.  Run `bash < name of file >.sh` in the terminal.

# Fedora

## What's installed
* [Bash-it](https://github.com/Bash-it/bash-it)
* Brave Browser
* git-lfs
* java-latest-openjdk-devel
* libreoffice
* scribus
* thunderbird
* timeshift  
* virt-manager
* xdotool

### Optional installs:
* Anaconda
* Brother Printer Driver
* Jetbrains Toolbox
* Slack
* Spotify
* VS Code

### If the machine has a NVIDIA GPU:
* RPM Fusion NVIDIA Drivers

## What's Removed:
* akregator
* calligra-sheets
* calligra-stage
* calligra-words
* falkon
* firefox
* juk
* k3b
* kamoso
* kmahjongg
* kmail
* kmines
* kmouth
* konqueror
* konversation
* kpat
* kruler
* ktorrent

## Settings and configurations
The script changes following settings and configurations on the system.

### .gitconfig
* Sets --global user.name
* Sets --global user.email
* Sets credential.helper timeout to 3600 seconds

### bash
* Uses [Bash-it](https://github.com/Bash-it/bash-it) color scheme.
* Sets the Bash-it theme to "powerline-plain"
* Sets the cursor a blinking ibeam

### VS Code
* Sets autosave to "afterDelay"
* Disables crash reporter
* Disables telemetry
* Sets color scheme to "Atom One Dark"
* Sets startup page to "none"
* Sets icons to material theme
* Sets font size to 13
* Extensions installed
    * Anaconda Extension Pack
    * Atom One Dark Theme
    * Auto Close Tag
    * Auto Rename Tag
    * Better Comments
    * Bracket Pair Colorizer 2
    * Code Spell Checker
    * colorize
    * Debugger for Java
    * Django
    * Docker
    * GitLens
    * Java Test Runner
    * Language Support for Java
    * Material Icon Theme
    * Path Intellisense
    * Prettier - Code formatter
    * Python
    * Settings Sync
    * shell-format
    * Visual Studio IntelliCode
    * YAML

### KDE
* Sets theme to "Breeze Dark"

## Manual Configuration

### Look and Feel
![Desktop](images/Fedora.png)

* Global Theme > Get New Global Themes... > Sweet
* Plasma Style > Get New Plasma Styles... > Sweet
* Icons > Get New Icons... > Papirus-Dark
* Wallpaper > Sweet-S4
* Login Screen > Sweet

### Tiling
* Kwin Scripts > Get New Scripts... > Tiling
* Kwin Scripts > "enable" Tiling Extension

### Shortcuts
The following allows for window management similar to i3.
* Right click on application menu > Keyoard Shortcuts > Alt+D
* Global Shortcuts > KDE Daemon > Launch Konsole > Alt+Return
* Global Shortcuts > KWin > Close Window > Alt+Shift+Q
* Global Shortcuts > KWin > Maximize Window > Alt+W
* Global Shortcuts > KWin > Minimize Window > Alt+Q
* Global Shortcuts > KWin > TILING: Cycle Rotations > Alt+E
* Global Shortcuts > KWin > TILING: Focus next tile > Alt+L
* Global Shortcuts > KWin > TILING: Focus previous tile > Alt+J
* Global Shortcuts > KWin > TILING: Swap Window With Master > Alt+S






# Pop!_OS

## What's installed
* apt-transport-https 
* [Bash-it](https://github.com/Bash-it/bash-it)
* Brave browser
* deja-dup
* gconf2 
* git-lfs 
* gnome-shell-extension-ubuntu-dock
* gnome-tweaks 
* gparted
* libappindicator1 
* libdbusmenu-gtk4  
* python3-distutils 
* python3-pip
* synaptic 
* tensorman 
* xdotool

### Optional installs:
* Anaconda
* Brother Printer Driver
* Jetbrains Toolbox
* KDE Desktop
* Scribus
* Slack
* Spotify
* Thunderbird
* VS Code


### If KDE desktop is selected:
* plasma-applet-redshift-control
* redshift

### If the machine has a NVIDIA GPU:
* system76-cuda-latest
* system76-cudnn-\*.\*

## What's Removed:
* firefox 
* geary
* gnome-weather 

### If KDE Desktop is installed:
* akregator
* dragonplayer
* gwenview
* imagemagick
* juk
* kate
* kcalc
* kmail
* kopete


## Settings and configurations
The script changes following settings and configurations on the system.

### .gitconfig
* Sets --global user.name
* Sets --global user.email
* Sets credential.helper timeout to 3600 seconds

### bash
* Uses [Bash-it](https://github.com/Bash-it/bash-it) color scheme.
* Sets the Bash-it theme to "powerline-plain"
* Sets the cursor a blinking ibeam

### VS Code
* Sets autosave to "afterDelay"
* Disables crash reporter
* Disables telemetry
* Sets color scheme to "Monokai"
* Sets startup page to "none"

### Gnome
* Creates a view desktop command
* Minimizes open windows by clicking the applications favorite icon
* Auto hides the dock to the bottem center of the screen
* Removes the trash icon from favorites
* Removes mounts icon from favorites
* Enables night light to run at all times
* Sets night light a temperature of 4000
* Changes the theme to "Pop-dark"
* Disables animations
* Shows the battery percentage
* Enables removal of trash and temp files after 30 days.
* Creates a desktop view
* Changes touch pad setting to "areas" click method.
* Sets custom enabled-extensions
* Sets the favorite bar to display Brave, Thunderbird, Spotify, Jetbrains Toolbox, VS Code, Files, and Show desktop icons.

### KDE
* Sets theme to "Breeze Dark"
